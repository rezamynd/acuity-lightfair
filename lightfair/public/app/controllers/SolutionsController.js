﻿'use strict';

(function () {

    var solutionsController = function ($scope, solutionsService, $timeout) {
        $scope.isEnter = false;
        $scope.isInit = false;
        $scope.isIdle = false;

        //var idleTimer,
        //    moveEvent = 'ontouchstart' in document.documentElement ? 'ontouchstart' : 'mousemove',
        //    onMove = function () {
        //        $scope.isIdle = false;
        //        $timeout.cancel(idleTimer);
        //        idleTO();
        //    },
        //    idleTO = function () {
        //        idleTimer = $timeout(function () {
        //            $scope.isIdle = true;
        //        }, 30000);
        //    };

        //window.addEventListener(moveEvent, onMove);

        $timeout(function(){
            $scope.isInit = true;
        },5000);

        $timeout(function () {
            $scope.isEnter = true;
        }, 500);

        solutionsService.getSolutions().then(function (data) {
            $scope.solutions = data;
        }, function (error) { console.log('Please try again later') });

    };

    lightfairApp.controller('SolutionsController', ['$scope', 'solutionsService', '$timeout',  solutionsController]);

}());

