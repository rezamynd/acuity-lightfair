﻿'use strict';

(function () {

    var projectPortfolioController = function ($scope, $http, contentService, imageService, $stateParams, $timeout) {
        $scope.image = imageService.getImage;

        contentService.get('project-portfolio').then(function (data) {
            $scope.content = data.projectPortfolio;
        }, function (error) { console.log('Please try again later') });
        $timeout(function () {
            $scope.entered = true;
        }, 500);

    };

    lightfairApp.controller('ProjectPortfolioController', ['$scope', '$http', 'contentService', 'imageService', '$stateParams', '$timeout', projectPortfolioController]);

}());

