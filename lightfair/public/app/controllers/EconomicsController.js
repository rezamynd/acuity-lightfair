﻿'use strict';

(function () {

    var economicsController = function ($scope, $http, contentService, $stateParams, imageService, $timeout) {
        $scope.image = imageService.getImage;
        $scope.asHtml = contentService.asHtml;
        contentService.get('economics').then(function (data) {
            $scope.content = data.economics;
            $timeout(function () {
                $scope.entered = true;
            }, 500);
        }, function (error) { console.log(error) });

    };

    lightfairApp.controller('EconomicsController', ['$scope', '$http', 'contentService', '$stateParams', 'imageService', '$timeout', economicsController]);

}());

