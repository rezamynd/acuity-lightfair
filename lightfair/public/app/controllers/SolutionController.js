﻿'use strict';

(function () {

    var solutionController = function ($scope, solutionsService, $rootScope, solutions,$stateParams, $state) {
        
        //put this in a directive
            $scope.showSolutionNav = false;

            $scope.toggleSolutionNav = function () {
                $scope.showSolutionNav = !$scope.showSolutionNav;
                $scope.$parent.myScroll['navScrollWrapper'].scrollTo(0,0,300);
            }

            $scope.currentPage = function (page) {
                return window.location.href.indexOf(page) >= 0;
            }
        //end

            $scope.go = function (state, params) {
                $state.go(state, { solution: params });
            };

        $scope.solutions = solutions;
        $scope.currentSolution = solutionsService.currentSolution();
        $scope.childView = $state.current.name.indexOf('solution.') >= 0;

        $rootScope.$on('$stateChangeStart', function (event, toState, fromState, fromParams) {
            $scope.childView = toState.name.indexOf('solution.') >= 0;
        });

    };

    lightfairApp.controller('SolutionController', ['$scope', 'solutionsService', '$rootScope', 'solutions', '$stateParams', '$state', solutionController]);

}());

