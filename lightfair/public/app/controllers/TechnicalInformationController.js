﻿'use strict';

(function () {

    var technicalInformationController = function ($scope, $http, contentService, imageService, $stateParams, $timeout) {
        $scope.image = imageService.getImage;
        $scope.entered = false;
        contentService.get('technical-information').then(function (data) {
            var _data = data.technicalInformation;

            if (!_data.product.length) {
                var _product = _data.product;
                _data.product = [];
                _data.product[0] = _product;
            }

            $scope.content = _data
            $timeout(function () {
                $scope.entered = true;
            }, 500);
            
        }, function (error) { console.log('Please try again later') });

        

    };

    lightfairApp.controller('TechnicalInformationController', ['$scope', '$http', 'contentService', 'imageService', '$stateParams', '$timeout', technicalInformationController]);

}());

