﻿'use strict';

(function () {

    var codeImplicationsController = function ($scope, $http, contentService, imageService, $stateParams, $timeout) {
        $scope.image = imageService.getImage;
        $scope.asHtml = contentService.asHtml;

        contentService.get('code-implications').then(function (data) {
            $scope.content = data.codeImplications;
            $timeout(function () {
                $scope.entered = true;
            }, 500);
        }, function (error) { console.log('Please try again later') });

    };

    lightfairApp.controller('CodeImplicationsController', ['$scope', '$http', 'contentService' , 'imageService', '$stateParams', '$timeout', codeImplicationsController]);

}());

