﻿'use strict';

(function () {

    var networkSolutionsController = function ($scope, $http, contentService, $stateParams, imageService, $sce, $timeout) {
        $scope.image = imageService.getImage;
        $scope.asHtml = contentService.asHtml;

        $scope.showVideo = false;
        $scope.playVideo = function () {
            $scope.showVideo = true;
            $scope.showModal = false;
            document.getElementById('nsVideo').play();
        }

        $scope.hideVideo = function () {
            document.getElementById('nsVideo').pause();
            $scope.showVideo = false;
        }

        $scope.selectedSolution = {};
        $scope.showModal = false;

        $scope.toggleModal = function (solution) {
            if (solution.image) {
                if (solution) {
                    $scope.selectedSolution = solution;
                } else {
                    $scope.selectedSolution = null;
                }
                $scope.showModal = !$scope.showModal;
            }
        };

        $scope.$on('$destroy', function() {
            var video = document.getElementById('nsVideo');
            if (video && video.readyState == 4) {
                $timeout(function () {
                    location.reload();
                }, 1000);
                
            }
        });

        contentService.get('network-solutions').then(function (data) {
            var _data = data.networkSolutions;

            if (!_data.solution.length) {
                var _solution = _data.solution;
                _data.solution = [];
                _data.solution[0] = _solution;
            }

            $scope.content = _data;
            $timeout(function () {
                $scope.entered = true;
            }, 500);

        }, function (error) { console.log('Please try again later') });
    };

    lightfairApp.controller('NetworkSolutionsController', ['$scope', '$http', 'contentService', '$stateParams', 'imageService', '$sce', '$timeout', networkSolutionsController]);

}());

