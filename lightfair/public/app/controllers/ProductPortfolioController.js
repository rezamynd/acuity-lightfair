﻿'use strict';

(function () {

    var productPortfolioController = function ($scope, $http, contentService, imageService, $stateParams, $timeout) {
        $scope.image = imageService.getImage;

        contentService.get('product-portfolio').then(function (data) {
            $scope.content = data.productPortfolio;

            var _data = data.productPortfolio;

            if (!_data.product.length) {
                var _product = _data.product;
                _data.product = [];
                _data.product[0] = _product;
            }
            $timeout(function () {
                $scope.entered = true;
            }, 500);

        }, function (error) { console.log('Please try again later') });

    };

    lightfairApp.controller('ProductPortfolioController', ['$scope', '$http', 'contentService', 'imageService', '$stateParams', '$timeout', productPortfolioController]);

}());

