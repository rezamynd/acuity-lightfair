﻿'use strict';

var lightfairApp = {};

(function () {

    lightfairApp = angular.module('lightfairApp', [
        'ngTouch',
        'ui.router',
        'ngAnimate',
        'ng-iscroll'
    ]).
    config(function ($stateProvider, $urlRouterProvider, $animateProvider) {
        $stateProvider
            .state('solutions', {
                url: '/solutions',
                templateUrl: '/app/views/solutions.html',
                controller: 'SolutionsController'
            })
            .state('solution', {
                url: '/solutions/:solution',
                templateUrl: '/app/views/solution.html',
                resolve: {
                    solutions: function (solutionsService) {
                        return solutionsService.getSolutions().then(function (data) {
                            return data;
                        }, function (error) { console.log(error) });
                    }
                },
                controller: 'SolutionController'
            })
            .state('solution.economics', {
                url: '/economics',
                templateUrl: '/app/views/economics.html',
                controller: 'EconomicsController'
            })
            .state('solution.network-solutions', {
                url: '/network-solutions',
                templateUrl: '/app/views/network-solutions.html',
                controller: 'NetworkSolutionsController'
            })
            .state('solution.technical-information', {
                url: '/technical-information',
                templateUrl: '/app/views/technical-information.html',
                controller: 'TechnicalInformationController'
            })
            .state('solution.code-implications', {
                url: '/code-implications',
                templateUrl: '/app/views/code-implications.html',
                controller: 'CodeImplicationsController'
            })
            .state('solution.project-portfolio', {
                url: '/project-portfolio',
                templateUrl: '/app/views/project-portfolio.html',
                controller: 'ProjectPortfolioController'
            })
            .state('solution.product-portfolio', {
                url: '/product-portfolio',
                templateUrl: '/app/views/product-portfolio.html',
                controller: 'ProductPortfolioController'
            });

        $urlRouterProvider.otherwise("/solutions");
        $animateProvider.classNameFilter(/section/);

    });

}());
