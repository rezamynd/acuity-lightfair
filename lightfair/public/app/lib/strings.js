﻿(function (String, O) {
    if (!String.friendlyUrl) {
        String.prototype.friendlyUrl = function (str) {
            return this.replace(/ /g, '').replace(/-/g, '').replace(/,/g, '').replace(/&/g, '').replace(/:/g, '').toLowerCase();
        };
    }
})(String, Object);