﻿'use strict';

(function () {

    var gallery = function (imageService, contentService, $animate) {

        return {
            restrict: 'AE',
            replace: true,
            transclude: true,
            scope: {
                items : '=',
            },
            link: function (scope) {
                
                scope.image = imageService.getImage;
                scope.asHtml = contentService.asHtml;

                scope.showDescription = true;
                scope.toggleDescription = function () {
                    scope.showDescription = !scope.showDescription;
                }; 

                scope.currentItem = -1;
                scope.changeItem = function (idx) {
                    if (idx != scope.currentItem) {
                        goTo(idx);
                    }
                }

                scope.$watch('items', function (items) {
                    if (items && items.length) {
                        goTo(0);
                    }
                });

                function goTo(idx) {
                    if (scope.currentItem >= 0) {
                        scope.items[scope.currentItem].visible = false;
                    }
                    scope.currentItem = idx;
                    scope.items[idx].visible = true;
                }
                
            },
            templateUrl : '/app/templates/gallery.html'
        }
    };
    
    lightfairApp.directive('gallery', ['imageService', 'contentService', '$animate', gallery]);

}());
