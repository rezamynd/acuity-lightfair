﻿'use strict';

(function () {

    var technicalInformationDocuments = function (imageService) {
        return {
            restrict: 'AE',
            replace: true,
            scope: {
                documents: '=',
                maxview : '='
            },
            link: function (scope, elem, attrs) {
                scope.image = imageService.getImage;

                var screens;

                function init() {
                    
                    for (var doc in scope.documents) {
                        scope.docs.push(scope.documents[doc]);
                    }

                    if (scope.docs.length > scope.maxview + 1) {
                        screens = Math.ceil(scope.docs.length / scope.maxview) - 1;
                        scope.currentIndex = 0;
                        scope.currentX = 0;

                        scope.$watch('currentIndex', function () {
                            scope.currentX = -(scope.currentIndex * window.innerWidth) + 'px';
                        });
                    }
                    
                };

                scope.docs = [];

                scope.next = function () {
                    scope.currentIndex < screens ? scope.currentIndex++ : scope.currentIndex = 0;
                };

                scope.prev = function () {
                    scope.currentIndex > 0 ? scope.currentIndex-- : scope.currentIndex = screens ;
                };

                scope.openDocument = function (document) {
                    window.location.pathname = scope.image(document);
                };

                var kickstart = scope.$watch('documents', function (items) {
                    if (items) {
                        init();
                        kickstart();
                    }
                });
                
            },
            templateUrl: '/app/templates/technicalInformationDocuments.html'
        }
    };

    lightfairApp.directive('techdocs', ['imageService', technicalInformationDocuments]);

}());
