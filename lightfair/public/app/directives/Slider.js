﻿'use strict';

(function () {

    var slider = function ($timeout) {
        return {
            restrict: 'AE',
            replace: true,
            scope: {
                solutions : '='
            },
            link: function (scope, elem, attrs) {

                var timer,
                    slideInterval;

                function init() {
                    var currentNav;
                    for (var solution in scope.solutions) {
                        scope.images.push(scope.solutions[solution]);
                    }

                    scope.currentIndex = 0;
                    scope.$watch('currentIndex', function () {
                        scope.images.forEach(function (image) {
                            image.visible = false;
                        });

                        //maybe don't do this.
                        if(currentNav){
                            currentNav.className = 'link-wrapper';
                        }
                        currentNav = document.getElementById('lnk_' + scope.images[scope.currentIndex].slug);
                        currentNav.className = 'link-wrapper active';

                        scope.images[scope.currentIndex].visible = true;
                    });

                    slideInterval = function () {
                        timer = $timeout(function () {
                            scope.next();
                            timer = $timeout(slideInterval, 5000);
                        }, 5000);
                    };

                    slideInterval();
                    scope.images[0].visible = true;
                    scope.$on('$destroy', function () {
                        $timeout.cancel(timer);
                    });
                    
                };

                scope.images = [];

                scope.next = function () {
                    scope.currentIndex < scope.images.length - 1 ? scope.currentIndex++ : scope.currentIndex = 0;
                };


                var kickstart = scope.$watch('solutions', function (items) {
                    if (items) {
                        init();
                        kickstart();
                    }
                });
                
            },
            templateUrl : '/app/templates/slider.html'
        }
    };

    lightfairApp.directive('slider', slider);

}());
