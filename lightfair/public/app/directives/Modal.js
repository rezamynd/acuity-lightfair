﻿'use strict';

(function () {

    var modal = function (imageService) {
        return {
            restrict: 'E',
            replace: true,
            transclude: true,
            scope: {
                show : '=',
            },
            link: function (scope) {
                scope.hide = function () {
                    scope.show = false;
                }
            },
            templateUrl : '/app/templates/modal.html'
        }
    };
    
    lightfairApp.directive('modal', ['imageService', modal]);

}());
