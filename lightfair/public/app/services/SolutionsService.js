﻿(function () {

    var solutionsService = function ($http, $q, imageService, $stateParams, $rootScope) {

        var solutionsFactory = {},
            serviceBaseUrl = '/data/solutions.xml',
            image = imageService.getImage,
            solutions,
            currentSolution;

        solutionsFactory.getSolutions = function () {
            var deferred = $q.defer();
            if(!solutions) {
                $http.get(serviceBaseUrl, { cache: true }).success(function (data) {
                    var _solutions = x2js.xml_str2json(data).solutions,
                        x = 0,
                        len = _solutions.solution.length,
                        _s;

                    solutions = {};

                    for (; x < len; x++) {
                        _s = _solutions.solution[x];
                        _s._background = image(_s._background, _s._name);
                        _s.slug = _s._name.friendlyUrl();
                        solutions[_s.slug] = _s;
                    };

                    deferred.resolve(solutions);
                });
            } else {
                deferred.resolve(solutions);
            }
            
            return deferred.promise;
        };

        $rootScope.$on('$stateChangeStart', function (event, toState, toParams, fromState, fromParams) {
            if (!currentSolution || (toParams.solution && (toParams.solution != currentSolution._name))) {
               currentSolution = solutions[toParams.solution];
            }
        });

        solutionsFactory.currentSolution = function () {
            if (!currentSolution && solutions) {
                currentSolution = solutions[$stateParams.solution];
            }
            return currentSolution;
        };

        return solutionsFactory;
    };

    lightfairApp.factory('solutionsService', ['$http', '$q', 'imageService', '$stateParams', '$rootScope', solutionsService]);

}());
