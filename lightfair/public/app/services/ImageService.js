﻿(function () {

    var imageService = function ($http, $q, $stateParams) {

        var imageFactory = {},
            imageBaseUrl = '/img/solutions/';

        imageFactory.getImage = function (filename, solution) {
            var currentSolution = solution || $stateParams.solution;

            var imagePath = imageBaseUrl + currentSolution.friendlyUrl() + '/';
            return imagePath + filename;
        }

        return imageFactory;
    };

    lightfairApp.factory('imageService', ['$http', '$q', '$stateParams', imageService]);

}());


