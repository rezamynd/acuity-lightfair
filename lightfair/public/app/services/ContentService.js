﻿(function () {

    var contentService = function ($http, $q, $stateParams, $sce) {

        var contentFactory = {},
            serviceBaseUrl = '/data/solutions/';
            
        contentFactory.asHtml = function (str) {
            if (str) {
                var _str = str;
                if (str.__cdata) {
                    _str = str.__cdata;
                }
                return $sce.trustAsHtml(_str);
            }
        }

        contentFactory.get = function (name) {
            var currentSolution = $stateParams.solution,
                deferred = $q.defer(),
                file = serviceBaseUrl + currentSolution.friendlyUrl() + '/' + name + '.xml';

            $http.get(file, { cache: false }).success(function (data) {
                deferred.resolve(x2js.xml_str2json(data));
            });
            return deferred.promise;
        };

        return contentFactory;
    };

    lightfairApp.factory('contentService', ['$http', '$q', '$stateParams', '$sce', contentService]);

}());

